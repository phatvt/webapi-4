package vn.com.fsoft.mtservice.api;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import vn.com.fsoft.mtservice.bean.data.UserRepo;
import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.controller.command.UserCommandController;
import vn.com.fsoft.mtservice.controller.query.UserQueryController;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.QueryResponseStatus;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.base.Status;
import vn.com.fsoft.mtservice.object.base.ValidationStatus;
import vn.com.fsoft.mtservice.object.constant.enumeration.FunctionEnum;
import vn.com.fsoft.mtservice.object.entities.RoleEntity;
import vn.com.fsoft.mtservice.object.entities.UserEntity;
import vn.com.fsoft.mtservice.object.form.UserForm;
import vn.com.fsoft.mtservice.object.models.UserAuthorityModel;
import vn.com.fsoft.mtservice.services.validator.UserValidator;
import vn.com.fsoft.mtservice.util.IncomingRestUtils;
import vn.com.fsoft.mtservice.util.JacksonUtils;
import vn.com.fsoft.mtservice.util.Views;

import java.util.List;

/**
 * 
 * @author hungxoan
 *
 */

@RestController("userAPI")
@RequestMapping("/api/user")
public class UserAPI extends BaseAPI {

    @Autowired
    private UserCommandController commandController;

    @Autowired
    private UserQueryController queryController;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private UserRepo userRepo;

    @GetMapping(value = "/_findOne/{userName}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> findOneImplementation(@PathVariable String userName,
                                                        RequestEntity<UserForm> requestEntity) {

        RepoStatus<List<UserEntity>> status = userRepo.findByCode(userName);

        if(!status.getCode().equals(HttpConstant.CODE_SUCCESS)) {
            return new ResponseEntity<String>(JacksonUtils.java2Json(new Status(status.getCode(),
                    status.getMessage())), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<String>(JacksonUtils.java2Json(new QueryResponseStatus<>(HttpConstant.CODE_SUCCESS,
                "found", status.getObject()), Views.Public.class), HttpStatus.OK);
    }

    @PostMapping(value = "/_query",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> queryImplementation(RequestEntity<UserForm> requestEntity) {

        ValidationStatus validationStatus = userValidator.reject(requestEntity, "query");
        if(validationStatus != null) {
            return queryController.reject(validationStatus);
        }

        IncomingRequestContext requestContext = IncomingRestUtils.getContext(requestEntity);

        return queryController.query(requestContext, FunctionEnum.WST_COLLECTION, requestEntity);
    }

    @GetMapping(value = "/_roles",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getRoles(RequestEntity<UserForm> requestEntity) {

        if(requestEntity == null) {

            return new ResponseEntity<String>(JacksonUtils.java2Json(
                    new ValidationStatus("400", "Request is undefined")),
                    HttpStatus.BAD_REQUEST);
        }

        RepoStatus<List<RoleEntity>> roleStatus = userRepo.getRoles();

        if(roleStatus.getObject() == null) {
            return new ResponseEntity<String>(JacksonUtils.
                    java2Json(new QueryResponseStatus("404", "not found")), HttpStatus.NOT_FOUND);
        }

        List<RoleEntity> roleEntityList = roleStatus.getObject();

        return new ResponseEntity<String>(JacksonUtils.
                java2Json(new QueryResponseStatus<RoleEntity>("200", "found", roleEntityList)), HttpStatus.OK);
    }

    @GetMapping(value = "/_authorities/{id}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getRoles(@PathVariable("id") Integer id,
            RequestEntity<UserForm> requestEntity) {

        RepoStatus<List<UserAuthorityModel>> repoStatus = userRepo.getUserAuthorities(id);

        if(repoStatus.getObject() == null) {
            return new ResponseEntity<String>(JacksonUtils.
                    java2Json(new QueryResponseStatus("202", "not found")),
                    HttpStatus.OK);
        }

        return new ResponseEntity<String>(JacksonUtils.
                java2Json(new QueryResponseStatus("200", "found", repoStatus.getObject())),
                HttpStatus.OK);
    }

    @PutMapping(value = "/_index",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> indexImplementation(RequestEntity<UserForm> requestEntity) {

        UserForm form = requestEntity.getBody();
        String type = form.getT();
        IncomingRequestContext requestContext = IncomingRestUtils.getContext(requestEntity);

        // validate
        ValidationStatus validationStatus = userValidator.reject(requestEntity, "command");
        if(validationStatus != null) {
            return commandController.reject(validationStatus);
        }

        if (type.equals("n")) {

            return commandController.save(requestContext, FunctionEnum.USER, requestEntity);
        }

        return commandController.update(requestContext, FunctionEnum.USER, requestEntity);
    }

    @DeleteMapping(value = "/_delete/{id}",
                  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> deleteImplementation(@PathVariable("id") Integer id,
                                                       RequestEntity<UserForm> requestEntity) {
        IncomingRequestContext requestContext = IncomingRestUtils.getContext(requestEntity);
        return commandController.delete(requestContext, id, FunctionEnum.USER, requestEntity);
    }

    @PostMapping(value = "/_bulk",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> deleteBulkImplementation(RequestEntity<UserForm> requestEntity) {

        // validate
        ValidationStatus validationStatus = userValidator.reject(requestEntity, "bulk");
        if(validationStatus != null) {
            return commandController.reject(validationStatus);
        }

        IncomingRequestContext requestContext = IncomingRestUtils.getContext(requestEntity);
        return commandController.bulk(requestContext, FunctionEnum.USER, requestEntity);
    }
}
