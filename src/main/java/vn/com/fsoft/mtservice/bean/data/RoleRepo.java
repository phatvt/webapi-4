package vn.com.fsoft.mtservice.bean.data;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import vn.com.fsoft.mtservice.bean.manager.ApplicationConfigManager;
import vn.com.fsoft.mtservice.bean.manager.MasterDataManager;
import vn.com.fsoft.mtservice.constants.CommonConstants;
import vn.com.fsoft.mtservice.constants.DatabaseConstants;
import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.base.ResultTransformer;
import vn.com.fsoft.mtservice.object.entities.AuthorityEntity;
import vn.com.fsoft.mtservice.object.entities.FunctionPermissionEntity;
import vn.com.fsoft.mtservice.object.entities.MasterDataTableEntity;
import vn.com.fsoft.mtservice.object.entities.RoleEntity;
import vn.com.fsoft.mtservice.object.form.RoleForm;
import vn.com.fsoft.mtservice.object.form.command.AuthorityCommandForm;
import vn.com.fsoft.mtservice.object.form.command.RoleCommandForm;
import vn.com.fsoft.mtservice.object.form.query.AuthorityQueryForm;
import vn.com.fsoft.mtservice.object.form.query.RoleQueryForm;
import vn.com.fsoft.mtservice.object.helper.QueryCallBack;
import vn.com.fsoft.mtservice.object.helper.QueryResult;
import vn.com.fsoft.mtservice.object.helper.TransformQueryResult;
import vn.com.fsoft.mtservice.object.helper.TransformerQueryCallBack;
import vn.com.fsoft.mtservice.object.models.RoleIDModel;
import vn.com.fsoft.mtservice.util.NumberUtils;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.*;

/**
 * 
 * @author hungxoan
 *
 */

@Repository("roleRepo")
public class RoleRepo extends BaseRepo<RoleEntity, ResultTransformer, RoleForm, RoleCommandForm> {

    private HibernateTemplate hibernateTemplate;

    @Value("${record.per.page}")
    private Integer recordPerPage;

    @Autowired
    private MasterDataManager masterDataManager;

    @Autowired
    private ApplicationConfigManager appConfigManager;

    public RoleRepo(@Autowired SessionFactory sessionFactory) {
        super(sessionFactory);
        this.hibernateTemplate = getHibernateTemplates();
    }

    @Override
    public RepoStatus<RoleEntity> findById(Integer id) {

        RoleEntity role = null;

        try {
            role = hibernateTemplate.get(RoleEntity.class, id);
        } catch(DataAccessException ex) {
            return null;
        }

        if(role == null) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        return new RepoStatus(HttpConstant.CODE_SUCCESS, "found", role);
    }

    @Override
    public RepoStatus<List<RoleEntity>> findByCode(String code) {

        String hql = "select r from RoleEntity r left join fetch r.authorities a ";
        hql += " left join fetch a.function f left join fetch a.permission p where r.code = :code";
        List<RoleEntity> entityList = null;
        try {

            entityList = (List<RoleEntity>) hibernateTemplate
                    .findByNamedParam(hql, "code", code);
        } catch (DataAccessException ex) {
            // log ex
            return new RepoStatus("500",
                    "Server got error. Please wait a moment and try it again later.");
        } catch (ObjectNotFoundException ex) {
            // log ex
            return new RepoStatus("404",
                    "No role linked to this role code");
        }

        if(CollectionUtils.isEmpty(entityList)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        return new RepoStatus<List<RoleEntity>>(HttpConstant.CODE_SUCCESS, "found",
                entityList);
    }

    public RepoStatus<List<FunctionPermissionEntity>> getAuthorityList() {

        String hql = "select a from FunctionPermissionEntity a left join fetch a.function f ";
        hql += "left join fetch a.permission p order by f.value1 asc,f.id asc, p.value1 asc ";

        List<FunctionPermissionEntity> authorities = null;
        try {
            authorities = (List<FunctionPermissionEntity>) hibernateTemplate.find(hql);
        } catch(DataAccessException ex) {
            // log ex
            return new RepoStatus("500",
                    "Server got error. Please wait a moment and try it again later.");
        }

        if(CollectionUtils.isEmpty(authorities)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        return new RepoStatus<List<FunctionPermissionEntity>>(HttpConstant.CODE_SUCCESS,
                "found", authorities);
    }

    public RepoStatus<List<MasterDataTableEntity>> getPermissions() {

        String hql = "select a from MasterDataTableEntity a where a.masterCategory = ? order by value1 asc ";
        List<MasterDataTableEntity> permissions = null;

        try {
            permissions = (List<MasterDataTableEntity>) hibernateTemplate.find(hql, "Permission");
        } catch(DataAccessException ex) {
            // log ex
            return null;
        }

        if(CollectionUtils.isEmpty(permissions)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        return new RepoStatus<List<MasterDataTableEntity>>(HttpConstant.CODE_SUCCESS,
                "found", permissions);
    }

    public RepoStatus<List<AuthorityEntity>> getUserAuthorities(Integer userId) {

        return null;
    }

    @Override
    public RepoStatus<QueryResult<RoleEntity>> query(RoleForm form) {
        Integer page = form.getP();
        if (null == page || 0 == page) {
            page = 1;
        }

        Integer pSize = form.getpSize();
        if (null == pSize || 0 == pSize) {
            pSize = recordPerPage;
        }

        RoleQueryForm queryForm = form.getQuery();

        String sqlFinal = CommonConstants.EMPTY;
        String sqlSelect = CommonConstants.EMPTY;
        String sqlWhere = CommonConstants.EMPTY;
        String sqlAuthorities = CommonConstants.EMPTY;

        sqlSelect = "SELECT r.id FROM " + DatabaseConstants.SCHEMA + ".roles r";

        List<String> parameterKey = new ArrayList<String>();
        List<Object> parameterValue = new ArrayList<Object>();

        if (StringUtils.isNotEmpty(queryForm.getCode())) {
            if (sqlWhere.length() > 0) {
                sqlWhere += " and ";
            }

            sqlWhere += " r.code like :code ";
            parameterKey.add("code");
            parameterValue.add(CommonConstants.PERCENT + queryForm.getCode() +
                    CommonConstants.PERCENT);
        }

        if (StringUtils.isNotEmpty(queryForm.getName())) {
            if (sqlWhere.length() > 0) {
                sqlWhere += " and ";
            }

            sqlWhere += " r.name like :name ";
            parameterKey.add("name");
            parameterValue.add(CommonConstants.PERCENT + queryForm.getName() +
                    CommonConstants.PERCENT);
        }

        if (StringUtils.isNotEmpty(queryForm.getDescription())) {
            if (sqlWhere.length() > 0) {
                sqlWhere += " and ";
            }

            sqlWhere += " r.description like :description ";
            parameterKey.add("description");
            parameterValue.add(CommonConstants.PERCENT + queryForm.getDescription() +
                    CommonConstants.PERCENT);
        }

        List<AuthorityQueryForm> authorityQueryForms = queryForm.getAuthorities();
        if(authorityQueryForms != null && authorityQueryForms.size() > 0) {

            Integer functionId = 0;
            List<Integer> permissionIds = null;
            Integer idx = 1;
            for(AuthorityQueryForm authorityQuery : authorityQueryForms) {

                functionId = authorityQuery.getFunction();
                permissionIds = authorityQuery.getPermissions();

                if(functionId != null && functionId != 0) {

                    if(idx > 1) {
                        sqlAuthorities += " and ";
                    }

                    sqlAuthorities += " exists ( select role_id from " + DatabaseConstants.SCHEMA + ".authority a where a.function_id = :function" + idx;

                    parameterKey.add("function" + idx);
                    parameterValue.add(functionId);

                    if(permissionIds != null && permissionIds.size() > 0) {
                        sqlAuthorities += " and a.role_permission_id in :permissions" + idx + " and r.id = a.role_id )";
                        parameterKey.add("permissions" + idx);
                        parameterValue.add(permissionIds);
                    } else {
                        sqlAuthorities += " and r.id = a.role_id )";
                    }
                }

                idx++;
            }
        }

        if(sqlWhere.length() > 0) {

            if(sqlAuthorities.length() > 0) {
                sqlWhere = " where " + sqlWhere + " and " + sqlAuthorities;
            } else {
                sqlWhere = " where " + sqlWhere;
            }
        } else {
            if(sqlAuthorities.length() > 0) {
                sqlWhere += " where " + sqlAuthorities;
            }
        }

        sqlFinal = sqlSelect + (sqlWhere.length() > 0 ? sqlWhere : "");

        String[] keySet = parameterKey.toArray(new String[parameterKey.size()]);
        Object[] valueSet = parameterValue.toArray();

        TransformerQueryCallBack<RoleIDModel> transformerQueryCallBack = new
                TransformerQueryCallBack<RoleIDModel>(sqlFinal, keySet, valueSet,
                0, 0, RoleIDModel.class, "SearchRoleID"
        );

        List<RoleIDModel> roleIdModelList = (List<RoleIDModel>) hibernateTemplate
                .execute(transformerQueryCallBack);

        if(CollectionUtils.isEmpty(roleIdModelList)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        List<Integer> roleIdList = new ArrayList<Integer>();
        for(RoleIDModel model : roleIdModelList) {
            roleIdList.add(model.getId());
        }

        String hql = "select distinct r from RoleEntity r left join fetch r.authorities a left join fetch ";
        hql += " a.function f left join fetch a.permission p where r.id in :roleIds ";
        hql += " order by r.createdTime desc";

        keySet = new String[] {"roleIds"};
        valueSet = new Object[] {roleIdList};
        QueryCallBack<RoleEntity> hbmQueryCallBack = new QueryCallBack<RoleEntity>(
                hql, keySet, valueSet, (page-1)*pSize, pSize
        );

        List<RoleEntity> entityList = null;
        entityList = (List<RoleEntity>) hibernateTemplate.execute(hbmQueryCallBack);

        if(CollectionUtils.isEmpty(entityList)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        Integer rowCount = hbmQueryCallBack.getRowCount();
        QueryResult<RoleEntity> queryResult = new QueryResult<RoleEntity>(rowCount - 1, entityList);

        return new RepoStatus(HttpConstant.CODE_SUCCESS, "found", queryResult);
    }

    @Override
    public RepoStatus<TransformQueryResult<ResultTransformer>> transformQuery(RoleForm form) {

        Integer page = form.getP();
        if (null == page || 0 == page) {
            page = 1;
        }

        Integer pSize = form.getpSize();
        if (null == pSize || 0 == pSize) {
            pSize = recordPerPage;
        }

        RoleQueryForm queryForm = form.getQuery();

        String hqlFinal = CommonConstants.EMPTY;
        String hqlSelect = CommonConstants.EMPTY;
        String hqlWhere = CommonConstants.EMPTY;
        String hqlAuthorities = CommonConstants.EMPTY;

        hqlSelect = "SELECT r.id FROM roles r";

        List<String> parameterKey = new ArrayList<String>();
        List<Object> parameterValue = new ArrayList<Object>();

        if (StringUtils.isNotEmpty(queryForm.getCode())) {
            if (hqlWhere.length() > 0) {
                hqlWhere += " and ";
            }

            hqlWhere += " r.code like :code ";
            parameterKey.add("code");
            parameterValue.add(CommonConstants.PERCENT + queryForm.getCode() +
                    CommonConstants.PERCENT);
        }

        if (StringUtils.isNotEmpty(queryForm.getName())) {
            if (hqlWhere.length() > 0) {
                hqlWhere += " and ";
            }

            hqlWhere += " r.name like :name ";
            parameterKey.add("name");
            parameterValue.add(CommonConstants.PERCENT + queryForm.getName() +
                    CommonConstants.PERCENT);
        }

        if (StringUtils.isNotEmpty(queryForm.getDescription())) {
            if (hqlWhere.length() > 0) {
                hqlWhere += " and ";
            }

            hqlWhere += " r.description like :description ";
            parameterKey.add("description");
            parameterValue.add(CommonConstants.PERCENT + queryForm.getDescription() +
                    CommonConstants.PERCENT);
        }

        List<AuthorityQueryForm> authorityQueryForms = queryForm.getAuthorities();
        if(authorityQueryForms != null && authorityQueryForms.size() > 0) {

            Integer functionId = 0;
            List<Integer> permissionIds = null;
            Integer idx = 1;
            for(AuthorityQueryForm authorityQuery : authorityQueryForms) {

                functionId = authorityQuery.getFunction();
                permissionIds = authorityQuery.getPermissions();

                if(functionId != null && functionId != 0) {

                    if(idx > 1) {
                        hqlAuthorities += " and ";
                    }

                    hqlAuthorities += " exists ( select role_id from authority a where a.function_id = :function" + idx;

                    parameterKey.add("function" + idx);
                    parameterValue.add(functionId);

                    if(permissionIds != null && permissionIds.size() > 0) {
                        hqlAuthorities += " and a.role_permission_id in :permissions" + idx + " and r.id = a.role_id )";
                        parameterKey.add("permissions" + idx);
                        parameterValue.add(permissionIds);
                    } else {
                        hqlAuthorities += " and r.id = a.role_id )";
                    }
                }

                idx++;
            }
        }

        if(hqlWhere.length() > 0) {

            if(hqlAuthorities.length() > 0) {
                hqlWhere = " where " + hqlWhere + " and " + hqlAuthorities;
            } else {
                hqlWhere = " where " + hqlWhere;
            }
        } else {
            if(hqlAuthorities.length() > 0) {
                hqlWhere += " where " + hqlAuthorities;
            }
        }

        hqlFinal = hqlSelect + (hqlWhere.length() > 0 ? hqlWhere : "");

        String[] keySet = parameterKey.toArray(new String[parameterKey.size()]);
        Object[] valueSet = parameterValue.toArray();

        TransformerQueryCallBack<RoleIDModel> transformerQueryCallBack = new
                TransformerQueryCallBack<RoleIDModel>(hqlFinal, keySet, valueSet,
                (page-1)*pSize, pSize, RoleIDModel.class, "SearchRoleID"
        );

        List<RoleIDModel> roleIdModelList = (List<RoleIDModel>) hibernateTemplate
                .execute(transformerQueryCallBack);

        if(CollectionUtils.isEmpty(roleIdModelList)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        List<Integer> roleIdList = new ArrayList<Integer>();
        for(RoleIDModel model : roleIdModelList) {
            roleIdList.add(model.getId());
        }

        String hql = "select r from RoleEntity r left join fetch r.authorities a left join fetch ";
        hql += " a.function f left join fetch a.permission p where r.id in :roleId ";
        keySet = new String[] {"roleId"};
        valueSet = new Object[] {roleIdList};
        QueryCallBack<RoleEntity> hbmQueryCallBack = new QueryCallBack<RoleEntity>(
                hql, keySet, valueSet, (page-1)*pSize, pSize
        );

        List<RoleEntity> entityList = null;
        entityList = (List<RoleEntity>) hibernateTemplate.execute(hbmQueryCallBack);

        if(CollectionUtils.isEmpty(entityList)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        Integer rowCount = hbmQueryCallBack.getRowCount();
        QueryResult<RoleEntity> queryResult = new QueryResult<RoleEntity>(rowCount, entityList);

        return new RepoStatus(HttpConstant.CODE_SUCCESS, "found", queryResult);
    }

    @Override
    public RepoStatus<Object> objectQuery(RoleForm form) {
        return null;
    }

    @Override
    @Transactional
    public RepoStatus<Integer> save(IncomingRequestContext context, RoleCommandForm commandForm) {

        AuthorityEntity authorityEntity = null;
        Set<AuthorityEntity> authorityEntitySet = new HashSet<AuthorityEntity>();
        String code = commandForm.getCode();
        String name = commandForm.getName();
        String description = commandForm.getDescription();
        List<AuthorityCommandForm> authorityCommandForms = commandForm.getAuthorities();

        // create role entity
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setCode(code);
        roleEntity.setName(name);
        roleEntity.setDescription(description);
        roleEntity.setCreatedBy(context.getUserId());
        roleEntity.setCreatedTime(new Date());

        // variables will be used in the for loop
        Integer functionId = 0;
        List<Integer> permissions = null;
        MasterDataTableEntity functionEntity = null;
        MasterDataTableEntity permissionEntity = null;
        List<MasterDataTableEntity> permissionEntityList = new ArrayList<MasterDataTableEntity>();


        if(!CollectionUtils.isEmpty(authorityCommandForms)) {

            for(AuthorityCommandForm form : authorityCommandForms) {

                functionId = form.getFunction();
                permissions = form.getPermissions();

                if(NumberUtils.isEmpty(functionId)) {
                    return new RepoStatus("409",
                            "Function/Permissions data are not valid");
                }

                functionEntity = masterDataManager.get(functionId);

                if(!CollectionUtils.isEmpty(permissions)) {

                    for(Integer permissionId : permissions) {

                        permissionEntity = hibernateTemplate.get(MasterDataTableEntity.class,
                                permissionId);

                        authorityEntity = new AuthorityEntity();
                        authorityEntity.setFunction(functionEntity);
                        authorityEntity.setPermission(permissionEntity);
                        authorityEntity.setRole(roleEntity);
                        authorityEntity.setCreatedBy(context.getUserId());
                        authorityEntity.setCreatedTime(new Date());
                        authorityEntitySet.add(authorityEntity);
                    }
                }

                functionEntity = null;
                permissionEntity = null;
            }
        }

        roleEntity.setAuthorities(authorityEntitySet);
        Serializable id = 0;
        try {
            id = hibernateTemplate.save(roleEntity);
        } catch (DataAccessException ex) {
            return new RepoStatus("500",
                    "Server got error. Please wait a moment and try it again later.");
        }

        return new RepoStatus("200", "acknowledged", id);
    }

    @Override
    @Transactional
    public RepoStatus<Integer> update(IncomingRequestContext context, RoleCommandForm commandForm) {

        AuthorityEntity authorityEntity = null;
        Set<AuthorityEntity> authorityEntitySet = new HashSet<AuthorityEntity>();
        Integer roleId = commandForm.getId();
        String code = commandForm.getCode();
        String name = commandForm.getName();
        String description = commandForm.getDescription();
        List<AuthorityCommandForm> authorityCommandForms = commandForm.getAuthorities();

        List<RoleEntity> roleEntityList = null;
        RoleEntity roleEntity = null;
        String hql = "select r from RoleEntity r left join fetch r.authorities a where r.id = :id";

        try {

            roleEntityList = (List<RoleEntity>) hibernateTemplate.findByNamedParam(hql, "id", roleId);
        } catch (DataAccessException ex) {
            // log ex
            return new RepoStatus("500",
                    "Server got error. Please wait a moment and try it again later.");
        }

        if(CollectionUtils.isEmpty(roleEntityList)) {
            return new RepoStatus("404",
                    "No role is linked to this identifier");
        }

        roleEntity = roleEntityList.get(0);

        roleEntity.setCode(code);
        roleEntity.setName(name);
        roleEntity.setDescription(description);
        roleEntity.setEditedBy(context.getUserId());
        roleEntity.setEditedTime(new Date());

        // variables will be used in the for loop
        Integer functionId = 0;
        List<Integer> permissions = null;
        MasterDataTableEntity functionEntity = null;
        MasterDataTableEntity permissionEntity = null;
        List<MasterDataTableEntity> permissionEntityList = new ArrayList<MasterDataTableEntity>();

        if(authorityCommandForms != null && authorityCommandForms.size() > 0) {

            for(AuthorityCommandForm form : authorityCommandForms) {

                functionId = form.getFunction();
                permissions = form.getPermissions();
                functionEntity = masterDataManager.get(functionId);

                if(!CollectionUtils.isEmpty(permissions)) {

                    for(Integer permissionId : permissions) {

                        permissionEntity = masterDataManager.get(permissionId);

                        authorityEntity = new AuthorityEntity();
                        authorityEntity.setFunction(functionEntity);
                        authorityEntity.setPermission(permissionEntity);
                        authorityEntitySet.add(authorityEntity);
                    }
                }
            }
        }

        // delete all authorities first

        Set<AuthorityEntity> oldAuthorities = roleEntity.getAuthorities();

//
//        if(!CollectionUtils.isEmpty(oldAuthorities)) {
//            for(AuthorityEntity authority : oldAuthorities) {
//                roleEntity.getAuthorities().remove(authority);
//            }
//        }
        try {
            hibernateTemplate.deleteAll(oldAuthorities);
        } catch (DataAccessException ex) {
            // log ex
            return new RepoStatus("500",
                    "Server got error. Please wait a moment and try it again later.");
        }

        // then add new authorities
        roleEntity.setAuthorities(authorityEntitySet);
        try {
            hibernateTemplate.update(roleEntity);
        } catch (DataAccessException ex) {
            // log ex
            return new RepoStatus("500",
                    "Server got error. Please wait a moment and try it again later.");
        }

        return new RepoStatus("200", "acknowledged", roleId);
    }

    @Override
    @Transactional
    public RepoStatus<Boolean> delete(Integer id) {

        if(NumberUtils.isEmpty(id)) {
            return new RepoStatus("409",
                    "Role identifier is invalid", Boolean.FALSE);
        }

        // find if any user is applied for this role
        String hql = "select r from RoleEntity r left join fetch r.profiles P where r.id = :id";
        List<RoleEntity> roleEntityList = null;
        try {

            roleEntityList = (List<RoleEntity>) hibernateTemplate
                    .findByNamedParam(hql, "id", id);
        } catch (DataAccessException ex) {
            return new RepoStatus("500",
                    "Server got error. Please wait a moment and try it again later.");
        } catch (ObjectNotFoundException ex) {
            // log ex
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,
                    "No role linked to this identifier");
        }

        RoleEntity roleEntity = null;
        if(!CollectionUtils.isEmpty(roleEntityList)){

            roleEntity = roleEntityList.get(0);
        } else {

            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,
                    "No role linked to this identifier");
        }

        if(!CollectionUtils.isEmpty(roleEntity.getProfiles())) {
            return new RepoStatus("409",
                    "There're some users that applied this role already. " +
                            "Please remove them first then you can delete this role properly",
                    Boolean.FALSE);
        }

        // perform a deletion
        RoleEntity role = null;
        try {
            role = hibernateTemplate.get(RoleEntity.class, id);
        } catch (DataAccessException ex) {
            return new RepoStatus("500",
                    "Server got error. Please wait a moment and try it again later.");
        } catch (ObjectNotFoundException ex) {
            // log ex
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,
                    "No role linked to this identifier", Boolean.FALSE);
        }

        if(null == role) {
            return new RepoStatus("404",
                    "No role linked to this role code", Boolean.FALSE);
        }

        // check system admin
        if(isSystemAdminRole(role.getCode())) {
            return new RepoStatus<Boolean>(HttpConstant.CODE_BAD_REQUEST,
                    "Unable to delete system admin role",
                    Boolean.FALSE);
        }

        try {
            hibernateTemplate.delete(role);
        } catch (DataAccessException ex) {
            // log ex
            return new RepoStatus("500",
                    "Server got error. Please wait a moment and try it again later.",
                    Boolean.FALSE);
        }

        return new RepoStatus("200", "acknowledged", Boolean.TRUE);
    }

    @Override
    @Transactional
    public RepoStatus<Boolean> deleteBulk(List<Integer> ids) {

        if(CollectionUtils.isEmpty(ids)) {
            return new RepoStatus("409",
                    "The identifier list should not be empty", Boolean.FALSE);
        }

        // find if any user is applied for these roles
        String hql = "select p from ProfileEntity p left join fetch p.roles r where r.id in :ids";
        List<RoleEntity> roleEntityList = null;
        try {

            roleEntityList = (List<RoleEntity>) hibernateTemplate
                    .findByNamedParam(hql, "ids", ids);
        } catch (DataAccessException ex) {
            return new RepoStatus("500",
                    "Server got error. Please wait a moment and try it again later.");
        } catch (ObjectNotFoundException ex) {
            // log ex
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,
                    "No role linked to these identifiers");
        }

        if(!CollectionUtils.isEmpty(roleEntityList)) {
            return new RepoStatus("409",
                    "There're some users that applied these roles already. " +
                            "Please remove them first then you can delete this role properly",
                    Boolean.FALSE);
        }

        hql = "select r from RoleEntity r where r.id in :ids";

        List<RoleEntity> entityList = (List<RoleEntity>) hibernateTemplate
                .findByNamedParam(hql, "ids", ids);

        for(RoleEntity entity : entityList) {

            // check system admin
            if(isSystemAdminRole(entity.getCode())) {
                return new RepoStatus<Boolean>(HttpConstant.CODE_BAD_REQUEST,
                        "Unable to delete system admin role",
                        Boolean.FALSE);
            }
        }

        if(!CollectionUtils.isEmpty(entityList)) {
            try {
                hibernateTemplate.deleteAll(entityList);
            } catch (DataAccessException ex) {
                // log ex
                return new RepoStatus("500",
                        "Server got error. Please wait a moment and try it again later.");
            }
        }

        return new RepoStatus("200", "acknowledged", Boolean.TRUE);
    }

    private Boolean isSystemAdminRole(String roleCode) {

        String systemAdminUser = appConfigManager.get("SystemAdminRole");
        if(roleCode.equals(systemAdminUser)) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }
}
