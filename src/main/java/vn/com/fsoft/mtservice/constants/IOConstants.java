package vn.com.fsoft.mtservice.constants;

/**
 * hungxoan
 */
public class IOConstants {

    public static class ImportConstants{

        // Import Requirement
        public static final String REQUIREMENT_CODE = "$REQUIREMENTCODE";
        public static final String REQUIREMENT_DETAIL = "$REQUIREMENTDETAIL";
        public static final String REQUIREMENT_TOPIC = "$REQUIREMENTTOPIC";
        public static final String END = "$end$";
        
      //Import WSTCollection
        public static final String COLLECTION_CODE = "$COLLECTION_CODE";
		public static final String CATEGORY = "$CATEGORY";
		public static final String SUBCATEGORY = "$SUBCATEGORY";
		public static final String REPORTINGLOGIC = "$WST_REPORTINGTOPIC";
		public static final String MATURITY = "$MATURITY";
		public static final String REQUIREMENT = "$REQUIREMENT_CODE";
		public static final String STANDARDCODE = "$STANDARD_CODE";
    }

    public static class ExportConstants{

        //Export Collection
        public static final String SEQUENCE = "$sequence";
        public static final String COLLECTION_CODE = "$collection_code";
        public static final String CATEGORY = "$category";
        public static final String SUBCATEGORY = "$subcategory";
        public static final String MATURITY = "$maturity";
        public static final String REPORTINGTOPIC = "$wst_reportingtopic";
        public static final String STANDARDCODE = "$standardcode";
        public static final String STANDARDTYPE = "$standardtype";
        public static final String REQUIREMENT_CODE = "$requirementcode";
        public static final String REQUIREMENT_DETAIL = "$requirementdetail";
        public static final String STATUS = "$status";

    }

}
