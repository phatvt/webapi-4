package vn.com.fsoft.mtservice.object.base;

/**
 * 
 * @author hungxoan
 *
 */
public class ValidationStatus extends Status {

    public ValidationStatus() {
    }

    public ValidationStatus(String code, String message) {
        super(code, message);
    }
}
