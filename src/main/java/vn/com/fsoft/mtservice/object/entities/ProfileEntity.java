package vn.com.fsoft.mtservice.object.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

import vn.com.fsoft.mtservice.constants.DatabaseConstants;
import vn.com.fsoft.mtservice.util.Views;

/**
 * 
 * @author hungxoan
 *
 */
@Entity(name = "ProfileEntity")
@Table(name = "profiles", schema = DatabaseConstants.SCHEMA)
public class ProfileEntity extends TrackingFieldEntity implements Serializable {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "profile_value_generator",
		    strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "profile_value_generator", schema = DatabaseConstants.SCHEMA,
			sequenceName = "profile_seq")
    private Integer id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private UserEntity user;

	@Column(name = "full_name")
	private String fullName;

	@Column(name = "address", length = 300)
	private String address;

	@Column(name = "phone")
	private String phone;

	@Column(name = "company")
	private String company;

//    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE},
//			fetch = FetchType.LAZY)
//    @JoinTable(schema = DatabaseConstants.SCHEMA, name = "profile_roles")
//	@Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)

	@ManyToMany(fetch = FetchType.LAZY,
			cascade ={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(schema = DatabaseConstants.SCHEMA, name = "profile_roles",
			joinColumns = {@JoinColumn(name = "profile_id")},
			inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Set<RoleEntity> roles = new HashSet<RoleEntity>();

	public ProfileEntity() {
		super();
	}

	@JsonView(Views.Public.class)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonView(Views.RelationBackward.class)
	public UserEntity getUserId() {
		return user;
	}

	public void setUserId(UserEntity user) {
		this.user = user;
	}

	@JsonView(Views.Public.class)
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@JsonView(Views.Public.class)
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@JsonView(Views.Public.class)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@JsonView(Views.Public.class)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonView(Views.Public.class)
	public Set<RoleEntity> getRoles() {
		return roles;
	}

	public void setRoles(Set<RoleEntity> roles) {
		this.roles = roles;
	}
}
