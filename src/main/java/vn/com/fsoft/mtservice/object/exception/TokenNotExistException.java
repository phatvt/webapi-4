package vn.com.fsoft.mtservice.object.exception;

/**
 * 
 * @author hungxoan
 *
 */
public class TokenNotExistException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TokenNotExistException() {
    }

    public TokenNotExistException(String message) {
        super(message);
    }
}
