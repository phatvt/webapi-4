package vn.com.fsoft.mtservice.object.form.command;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * 
 * @author hungxoan
 *
 */

@JsonIgnoreProperties( ignoreUnknown = true)
public class RoleCommandForm extends RestCommandForm {

    private List<Integer> ids;
    private Integer id;

    private String code;
    private String name;
    private String description;

    private List<AuthorityCommandForm> authorities;

    public RoleCommandForm() {
    }

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AuthorityCommandForm> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<AuthorityCommandForm> authorities) {
        this.authorities = authorities;
    }
}
