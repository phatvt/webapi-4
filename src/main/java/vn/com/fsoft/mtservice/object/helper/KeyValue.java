package vn.com.fsoft.mtservice.object.helper;

import com.fasterxml.jackson.annotation.JsonView;

import vn.com.fsoft.mtservice.util.Views;

/**
 * 
 * @author hungxoan
 *
 */
public class KeyValue {
    private Integer key;
    private String value;

    public KeyValue() {
    }

    public KeyValue(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    @JsonView(Views.Public.class)
    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    @JsonView(Views.Public.class)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
