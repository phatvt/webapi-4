/**
 * 
 */
package vn.com.fsoft.mtservice.object.tokenizing;

import java.security.SecureRandom;

/**
 * @author hungxoan
 *
 */
public class Tokenizor {
	
	private static final String DICTIONARY = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static final Integer TOKEN_LENGTH = 128;
    
    public static String generateToken() {

        StringBuilder sb = new StringBuilder(TOKEN_LENGTH);

        SecureRandom random = new SecureRandom();
        for( int i = 0; i < TOKEN_LENGTH; i++ )
            sb.append(DICTIONARY.charAt(random.nextInt(DICTIONARY.length())));
        return sb.toString();
    }
}
