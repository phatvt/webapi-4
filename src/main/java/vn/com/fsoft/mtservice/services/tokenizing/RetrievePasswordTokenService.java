package vn.com.fsoft.mtservice.services.tokenizing;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import vn.com.fsoft.mtservice.object.exception.TokenNotExistException;
import vn.com.fsoft.mtservice.util.NumberUtils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author hungxoan
 *
 */

@Component("retrievePasswordTokenService")
public class RetrievePasswordTokenService {

    @Value("${retrieve.password.timeout.unit}")
    private Integer timeoutUnit;

    @Value("${retrieve.password.timeout.timeunit}")
    private String timeoutTimeUnit;

    private Map<String, Integer> tokenDatabase = new HashMap<String, Integer>();
    private LoadingCache<String, Integer> tokens;
    private final String INIT_SEQUENCE = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private final Integer TOKEN_LENGTH = 123;
    private final Integer MAX_ELEMENTS = 10000;
    private final Integer START = 11111;
    private Integer counter = 11111;

    public RetrievePasswordTokenService() {

        Integer unit = 0;
        TimeUnit timeUnit = null;

        if(timeoutUnit == null) {
            unit = 24;
        }

        if(timeoutTimeUnit == null) {
            timeUnit = TimeUnit.HOURS;
        } else {
            if(timeoutTimeUnit.toLowerCase().trim().equals("m")) {
                timeUnit = TimeUnit.MINUTES;
            } else if(timeoutTimeUnit.toLowerCase().trim().equals("d")) {
                timeUnit = TimeUnit.DAYS;
            }
        }

        tokens = CacheBuilder.newBuilder()
             .concurrencyLevel(4)
//             .weakKeys()
             .maximumSize(MAX_ELEMENTS)
             .expireAfterWrite(unit, timeUnit)
             .build(new CacheLoader<String, Integer>() {
                 @Override
                 public Integer load(String s) throws Exception {
                     Integer value = tokenDatabase.get(s);
                     if (value == null) {
                         throw new TokenNotExistException("Token do not exist");
                     }
                     tokenDatabase.remove(s);
                     return value;
                 }
             });
    }

    private String generateToken() {

        StringBuilder sb = new StringBuilder( TOKEN_LENGTH );

        // cycling counter
        if(counter >= (MAX_ELEMENTS*5 + START)) {
            counter = START;
        }

        // get unique prefix
        for(int i = 0; i < 5; i++) {
            sb.append(INIT_SEQUENCE.charAt(counter%(INIT_SEQUENCE.length())));
            counter++;
        }

        SecureRandom SECURE_RANDOM = new SecureRandom();
        for( int i = 0; i < TOKEN_LENGTH; i++ )
            sb.append(INIT_SEQUENCE.charAt(SECURE_RANDOM.nextInt(INIT_SEQUENCE.length())));
        return sb.toString();
    }

    public String needAToken(Integer userId) {

        String token = generateToken();
        tokenDatabase.put(token, userId);
        try {
            userId = tokens.get(token);
        } catch (ExecutionException ex) {
            // log ex
        }
        return token;
    }

    public Integer getFromDatabase(String token) {
        return tokenDatabase.get(token);
    }

    public synchronized Boolean isValid(String token) {

        Integer cachedUserId = null;
        try {
            cachedUserId = tokens.get(token);
        } catch (Exception ex) {
            if (ex instanceof TokenNotExistException){
                return Boolean.FALSE;
            }
        }

        if(NumberUtils.isEmpty(cachedUserId)) {
            return Boolean.FALSE;
        }

//        tokenDatabase.remove(token);
        return Boolean.TRUE;
    }

    public Integer accessToken(String token){
        if (isValid(token)) {
            Integer userId = null;
            userId = tokenDatabase.get(token);
            tokens.invalidate(token);
//            tokenDatabase.remove(token);
            return userId;
        }

        return null;
    }

    public Integer getUserId(String token) {
        Integer userId = null;
        try {
            userId = tokens.get(token);
        } catch (ExecutionException ex) {
            // log ex
        }

        return userId;
    }
}
