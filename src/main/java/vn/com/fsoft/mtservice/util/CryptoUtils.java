package vn.com.fsoft.mtservice.util;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;

import vn.com.fsoft.mtservice.constants.SecurityConstants;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 
 * @author hungxoan
 *
 */
public class CryptoUtils {

    public static String digest(String nativeInput, String hashingAlgorithm) {

        if(StringUtils.isEmpty(hashingAlgorithm)) {

            hashingAlgorithm = SecurityConstants.DEFAULT_HASHING_ALGORITHM;
        }

        MessageDigest messageDigest = null;
        try {

            messageDigest = MessageDigest.getInstance(hashingAlgorithm);
        } catch(NoSuchAlgorithmException ex) {

            // log exception
        }

        byte[] hash = messageDigest.digest(nativeInput.getBytes(StandardCharsets.UTF_8));
        return Hex.encodeHexString(hash);
    }
}
