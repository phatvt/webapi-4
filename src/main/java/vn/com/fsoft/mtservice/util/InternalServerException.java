////////////////////////////////////////////////////////////
// InternalServerException.java
// Version: 1.0
// Author: (FPT) PhatVT1
// Create date: 2018/05/26
// Update date: -
////////////////////////////////////////////////////////////
package vn.com.fsoft.mtservice.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
/**
 * 
 * @author hungxoan
 *
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private String message;

    public InternalServerException(String message) {
        super(String.format("Internal Server Error : '%s'", message));
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}